module salsa.debian.org/mdosch/go-sendxmpp

go 1.13

require (
	github.com/gabriel-vasile/mimetype v1.4.0
	github.com/mattn/go-xmpp v0.0.0-20211029151415-912ba614897a
	github.com/pborman/getopt/v2 v2.1.0
	golang.org/x/net v0.0.0-20220127200216-cd36cc0744dd // indirect
	salsa.debian.org/mdosch/xmppsrv v0.1.0
)
