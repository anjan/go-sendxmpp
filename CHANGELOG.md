# Changelog

##[Unreleased]

## [v0.1.3]
### Changed
- Rename files to use a limited character set (alpha numerical and some extra characters) file name before uploading. Workaround for https://github.com/mattn/go-xmpp/issues/132

## [v0.1.2]
### Changed
- Use xml.Marshal to safely build HTTP Upload request.
- Use salsa.debian.org/mdosch/xmppsrv for SRV lookups.

## [v0.1.1]
### Changed
- Xml-escape file name in http-upload.
- Xml-escape mimetype in http-upload.

## [v0.1.0]
### Added
- Initial release
