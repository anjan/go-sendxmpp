go-sendxmpp(5) -- A little tool to send messages to an XMPP contact or MUC.
====

## LOCATION

The account data is expected at ~/.config/go-sendxmpp/sendxmpprc (preferred) or ~/.sendxmpprc
(for compatibility with the original perl sendxmpp) if no other configuration file location is
specified with -f or --file. The configuration file is expected to be in the following format:

## FORMAT

username: [<your_jid>]    
jserver: [<jabber_server>]    
port: [<jabber_port>]    
password: [<your_jabber_password>]    
eval_password: [<command_to_unlock_your_password>]
resource: [<your_resource>]

## REQUIRED SETTINGS

If all necessary settings are supplied as command line arguments no config file is needed at all.
Setting `jserver` and `port` might not be necessary depending on the used server.    
You should either use a password manager and the setting `eval_password` or add
your password in plaintext to the config file with the setting `password`.

## AUTHOR

Written by Martin Dosch.

## REPORTING BUGS


Report bugs at [https://salsa.debian.org/mdosch/go-sendxmpp/issues](https://salsa.debian.org/mdosch/go-sendxmpp/issues).

## COPYRIGHT

Copyright (c) 2018 - 2021 Martin Dosch
License: BSD 2-clause License

## SEE ALSO

go-sendxmpp(1), sendxmpp(1)
