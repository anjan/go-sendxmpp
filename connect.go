// Copyright 2021 Martin Dosch.
// Use of this source code is governed by the BSD-2-clause
// license that can be found in the LICENSE file.package main

package main

import (
	"fmt"
	"net"
	"strings"

	"github.com/mattn/go-xmpp"        // BSD-3-Clause
	"salsa.debian.org/mdosch/xmppsrv" // BSD-2-Clause
)

func connect(options xmpp.Options, directTLS bool) (*xmpp.Client, error) {

	// Look up SRV records if server is not specified manually.
	if options.Host == "" {
		server := options.User[strings.Index(options.User, "@")+1:]
		// Look up xmpp-client SRV records.
		srvMixed, err := xmppsrv.LookupClient(server)
		if len(srvMixed) > 0 && err == nil {
			for _, adr := range srvMixed {
				if !directTLS && adr.Type != "xmpps-client" {
					// Use StartTLS
					options.NoTLS = true
					options.StartTLS = true
					options.Host = net.JoinHostPort(adr.Target, fmt.Sprint(adr.Port))
					// Connect to server
					client, err := options.NewClient()
					if err == nil {
						return client, err
					}
				} else if adr.Type == "xmpps-client" {
					// Use direct TLS
					options.NoTLS = false
					options.StartTLS = false
					options.Host = net.JoinHostPort(adr.Target, fmt.Sprint(adr.Port))
					// Connect to server
					client, err := options.NewClient()
					if err == nil {
						return client, err
					}

				}
			}
		}
		// Try port 5222 if no xmpp-client SRV records are provided.
		options.NoTLS = true
		options.StartTLS = true
		options.Host = net.JoinHostPort(server, "5222")
		// Connect to server
		client, err := options.NewClient()
		return client, err
	}
	// Connect to server
	client, err := options.NewClient()
	return client, err
}
